#!/bin/sh

if [ $(id -u) != 0 ]; then
    echo "Running with sudo..."
    exec sudo "$0" "$@"
    exit 1
fi

DIR=$(dirname "$0")
OUTDIR=$DIR/output/
SSH_ADDR=root@192.168.1.132

cleanup() {
    umount "$OUTDIR/mnt"
}


mkdir -p "$OUTDIR/mnt"

trap cleanup EXIT

mount -o loop,ro "$OUTDIR/images/rootfs.ext2" "$OUTDIR/mnt" || exit
rsync --exclude "/etc/shadow" --exclude "/tmp" --exclude "/dev" --exclude "/proc" --exclude "/sys" --exclude "/etc/random-seed" -i -crlpgoD "$OUTDIR/mnt/" "$SSH_ADDR:/"

