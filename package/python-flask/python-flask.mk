################################################################################
#
# python-flask
#
################################################################################

PYTHON_FLASK_VERSION = 0.10.1
PYTHON_FLASK_SOURCE = Flask-$(PYTHON_FLASK_VERSION).tar.gz
PYTHON_FLASK_SITE = http://pypi.python.org/packages/source/F/Flask
PYTHON_FLASK_LICENSE = BSD
PYTHON_FLASK_DEPENDENCIES = python-itsdangerous host-python-itsdangerous \
                            python-jinja2 host-python-jinja2 \
                            python-werkzeug host-python-werkzeug
PYTHON_FLASK_SETUP_TYPE = setuptools

$(eval $(python-package))
$(eval $(host-python-package))
