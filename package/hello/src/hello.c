#include <stdio.h>

void gpio_export(int n) {
    FILE *f;
    printf("gpio_export(%d)\n", n);
    f = fopen("/sys/class/gpio/export", "w");
    fprintf(f, "%d\n", n);
    fclose(f);
}

void gpio_set_direction(int gpio, const char *direction) {
    char filename[32];
    FILE *f;


    snprintf(filename, sizeof(filename), "/sys/class/gpio/gpio%d/direction", gpio);
    f = fopen(filename, "w");
    fprintf(f, "%s\n", direction);
    fclose(f);
}

void gpio_set_value(int gpio, int val) {
    char filename[32];
    snprintf(filename, sizeof(filename), "/sys/class/gpio/gpio%d/value", gpio);
    FILE *f = fopen(filename, "w");
    fprintf(f, "%d\n", val);
    fclose(f);
}

#define GREEN_LED 1
#define RED_LED 2
#define BLUE_LED 4

int main(void) {
    int rgb[] = { RED_LED, GREEN_LED, BLUE_LED };
    int i;
    for ( i = 0; i < 3; i ++) {
        gpio_export(rgb[i]);
        gpio_set_direction(rgb[i], "out");
        gpio_set_value(rgb[i], 0);
    }
}
