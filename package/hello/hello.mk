################################################################################
#
# hello
#
################################################################################

HELLO_VERSION = 1.0
HELLO_SITE = $(TOPDIR)/package/hello/src
HELLO_SITE_METHOD = local

HELLO_CONF_OPTS = CC="$(TARGET_CC)" CFLAGS="$(TARGET_CFLAGS)" LDFLAGS="$(TARGET_LDFLAGS)"

define HELLO_BUILD_CMDS
	$(TARGET_CONFIGURE_OPTS) $(MAKE) $(HELLO_CONF_OPTS) -C $(@D)
endef

define HELLO_INSTALL_TARGET_CMDS
	$(INSTALL) -m 0755 -D $(@D)/hello $(TARGET_DIR)/usr/bin/hello
endef

$(eval $(generic-package))
