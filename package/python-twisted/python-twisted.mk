################################################################################
#
# python-twisted
#
################################################################################

PYTHON_TWISTED_VERSION = 13.2.0
PYTHON_TWISTED_SOURCE = Twisted-$(PYTHON_TWISTED_VERSION).tar.bz2
PYTHON_TWISTED_SITE = http://pypi.python.org/packages/source/T/Twisted
PYTHON_TWISTED_LICENSE = MIT
PYTHON_TWISTED_DEPENDENCIES = python-zopeinterface host-python-zopeinterface
PYTHON_TWISTED_SETUP_TYPE = setuptools

$(eval $(python-package))
$(eval $(host-python-package))
