################################################################################
#
# python-werkzeug
#
################################################################################

PYTHON_WERKZEUG_VERSION = 0.9.4
PYTHON_WERKZEUG_SOURCE = Werkzeug-$(PYTHON_WERKZEUG_VERSION).tar.gz
PYTHON_WERKZEUG_SITE = http://pypi.python.org/packages/source/W/Werkzeug
PYTHON_WERKZEUG_LICENSE = BSD
PYTHON_WERKZEUG_SETUP_TYPE = setuptools

$(eval $(python-package))
$(eval $(host-python-package))
