RGBLED_VERSION = 1.0
RGBLED_SITE = $(TOPDIR)/package/rgbled/driver
RGBLED_SITE_METHOD = local
RGBLED_LICENSE = GPLv3+

RGBLED_DEPENDENCIES = linux

define RGBLED_BUILD_CMDS
	$(MAKE) -C $(LINUX_DIR) $(LINUX_MAKE_FLAGS) M=$(@D)
endef

define RGBLED_INSTALL_TARGET_CMDS
	$(MAKE) -C $(LINUX_DIR) $(LINUX_MAKE_FLAGS) M=$(@D) modules_install
endef


$(eval $(generic-package))
