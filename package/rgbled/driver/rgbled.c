/*
 * Ce module éteind la LED initialement puis un timer est lancé toute les PWM_PERIOD_NS / 256 * ACCURACY ns.
 * A l'expiration du timer, la fonction update_rgb_gpio va mettre à jour l'état de chaque composante de la LED multicolore : 
 *   - Une variable 'time', comprise entre 0 et 255, est alors augmentée de ACCURACY à chaque appel (modulo 256).
 *   - Pour chaque composante, on allume la LED correspondante si la variable 'time' est inférieure à la luminosité souhaitée de cette composante.
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/device.h>
#include <linux/gpio.h>
#include <linux/hrtimer.h>
#include <linux/ktime.h>
#include <linux/fs.h>
#include <linux/interrupt.h>
#include <asm/uaccess.h>

#define US_TO_NS(x) (x * 1E3L)
#define MS_TO_NS(x) (x * 1E6L)
#define PWM_PERIOD_NS US_TO_NS(10L)
#define ACCURACY 16
#define MIN(a, b) ((a) > (b) ? (b) : (a))

/* Prototyes */

/* Fonction appelée à l'ouverture du fichier de commande (ne fait rien). */
static int rgbled_open(struct inode *i, struct file *f);

/* Fonction appelée à la fermeture du fichier de commande (ne fait rien). */
static int rgbled_release(struct inode *i, struct file *f);

/* Fonction appelée lors de la lecture du fichier de commande.
 * Elle transmet la valeur actuelle de la LED au format hexadécimal suivi d'un saut de ligne */
static ssize_t rgbled_read(struct file *f, char *buf, size_t count, loff_t *f_pos);

/* Fonction appelée lors de l'écriture du fichier de commande.
 * Elle récupère la valeur hexadécimale (6 caractères)  à donner à la LED puis met à jour son état.
 * Les caractères surnuméraires sont simplement ignorés.
 */
static ssize_t rgbled_write(struct file *f, const char *buf, size_t count, loff_t *f_pos);

/* Fonction appelée à l'exécution du tasklet permettant d'éteindre la LED lors de l'appui sur le bouton */
static void tasklet_func_off(unsigned long data);

/* Fonction de gestion de l'interruption déclenchée par un changement d'état du bouton.
 * Elle lance un un tasklet pour réaliser l'action.
 */
static irqreturn_t irq_button (int irq, void *data);

/* Fonction appelée à l'expiration du timer gérant la PWM */
static enum hrtimer_restart pwm_hrtimer_callback(struct hrtimer *timer);

/* Met à jour l'état de la LED à partir de la chaine de caractère représentant l'intensité des 3 LED */
static void set_rgb(char *buffer);

/* Met à jour l'état de la LED depuis l'état stocké dans le buffer interne */
static void update_rgb_from_buffer(void);

/* Change les valeurs des GPIOs pour refléter l'état des LEDs */
static void update_rgb_gpio(void);

/* Convertit un nombre hexadécimal sur deux chiffres en entier */
static int hex2dec(char *buffer);


/* Indice de chaque élément dans le tableau `gpios` */
enum {
  RED_LED     = 0,
  GREEN_LED   = 1,
  BLUE_LED    = 2,
  GPIO_BUTTON = 3,
};

/* Définition des GPIOs utilisés */
static struct gpio gpios[] = {
    { 2, GPIOF_OUT_INIT_HIGH, "Red LED"   },
    { 1, GPIOF_OUT_INIT_HIGH, "Green LED" },
    { 4, GPIOF_OUT_INIT_HIGH, "Blue LED"  },
    { 5, GPIOF_IN,            "Button"    },
};

/* Définition des fonctions de réponse aux appels systèmes */
static struct file_operations fops = {
  .owner    = THIS_MODULE,
  .read     = rgbled_read,
  .write    = rgbled_write,
  .open     = rgbled_open,
  .release  = rgbled_release
};

/* Définition du tasklet gérant l'extinction de la LED lors de l'appui sur le bouton */
static struct tasklet_struct tasklet_off = { NULL, 0, ATOMIC_INIT(0), tasklet_func_off, 0 };

/* Variables d'état */
static struct {
  int r;                    /* Luminosité de la LED rouge (0-255) */
  int g;                    /* Luminosité de la LED verte (0-255) */
  int b;                    /* Luminosité de la LED bleue (0-255) */
  int time;                 /* Temps courant utilisé pour la PWM */
  struct hrtimer pwm_timer; /* Timer pour la PWM */
  char data_buffer[16];     /* Représentation hexadécimale de l'état de la LED */
  struct class *class;      /* Classe de notre fichier périphérique */
  struct device *device;    /* Fichier de périphérique */
  int irq;                  /* IRQ pour la détection de changement d'état du bouton */
  int major;
} state;


static void tasklet_func_off(unsigned long data) {
  set_rgb("000000");
}

static irqreturn_t irq_button (int irq, void *data) {
  tasklet_schedule(&tasklet_off);
  return IRQ_HANDLED;
}

static int __init rgbled_init(void)
{
  int err;
  ktime_t ktime;

  printk(KERN_INFO "Loading RGB LED driver\n");

  state.major = register_chrdev(0, "rgbled", &fops);

  if (state.major < 0) {
    printk(KERN_INFO "rgbled: cannot obtain major number\n");
    return state.major;
  }

  state.class = class_create(THIS_MODULE, "chardrv");

  if (IS_ERR(state.class)) {
    printk(KERN_ERR "rgbled: class_create failed\n");
    return PTR_ERR(state.class); // TODO: nettoyage
  }

  state.device = device_create(state.class, NULL, MKDEV(state.major, 0), NULL, "rgbled");
  if (IS_ERR(state.device)) {
    printk(KERN_ERR "rgbled: device_create failed\n");
    return PTR_ERR(state.device); // TODO : nettoyage
  }

  err = gpio_request_array(gpios, ARRAY_SIZE(gpios));
  if (err) {
    printk(KERN_ERR "rgb_led: error requesting GPIOs\n");
    return err;
  }

  state.irq = gpio_to_irq(gpios[GPIO_BUTTON].gpio);
  if (state.irq < 0) {
    printk(KERN_ERR "rgbled: failed getting GPIO IRQ number\n");
    return state.irq;
  }

  err = request_irq(state.irq, irq_button, IRQF_TRIGGER_RISING | IRQF_TRIGGER_FALLING, "Button", NULL);

  if (err) {
    printk(KERN_ERR "rgbled: error requesting IRQ\n");
    return err;
  }

  set_rgb("000000");

  /* Rafraichit la couleur de la led de façon à respecter une période de PWM_PERIOD_NS ns */
  ktime = ktime_set(0, PWM_PERIOD_NS / 256 * ACCURACY);

  hrtimer_init(&state.pwm_timer, CLOCK_MONOTONIC, HRTIMER_MODE_REL);

  state.pwm_timer.function = &pwm_hrtimer_callback;

  hrtimer_start(&state.pwm_timer, ktime, HRTIMER_MODE_REL);

  return 0;
}

static void __exit rgbled_exit(void)
{
  printk(KERN_INFO "Unloading RGB LED driver\n");

  if (!IS_ERR(state.device)) {
    device_destroy(state.class, MKDEV(state.major, 0));
  }

  if (!IS_ERR(state.class)) {
    class_destroy(state.class);
  }

  if (state.irq > 0) {
    free_irq(state.irq, NULL);
  }

  hrtimer_cancel(&state.pwm_timer);
  tasklet_kill(&tasklet_off);

  set_rgb("000000");
  update_rgb_gpio();

  gpio_free_array(gpios, ARRAY_SIZE(gpios));
}

static int rgbled_open(struct inode *i, struct file *f)
{
  return 0; // success
}

static int rgbled_release(struct inode *i, struct file *f)
{
  return 0; // success
}

static ssize_t rgbled_read(struct file *f, char *buf, size_t count, loff_t *f_pos)
{
  ssize_t nb; /* nombre de caractères à lire */

  /* Si la position actuelle dans le fichier dépasse les 6 caractères hexadécimaux + le caractère de fin de ligne,
   * il n'y a plus rien à lire, on renvoie donc 0*/

  if (*f_pos >= 7) {
    return 0;
  }

  /* il reste à lire (7 - *f_pos) caractères, que l'on plafonne à la taille du buffer de lecture */
  nb = MIN(7 - *f_pos, count);

  state.data_buffer[6] = '\n';

  if (copy_to_user(buf, state.data_buffer + *f_pos, nb) != 0) {
    return -EFAULT;
  }

  *f_pos += nb;

  return nb;
}

static ssize_t rgbled_write(struct file *f, const char *buf, size_t count, loff_t *f_pos)
{
  ssize_t nb; /* nombre de caractères à écrire */
  
  /* Si la position actuelle dans le fichier dépasse les 6 caractères hexadécimaux,
   * on a dépassé la taille des données et on ignore le reste */
  if (*f_pos >= 6) {
    return count;
  }
 

  /* il y a encore de la place pour (6 - *f_pos) caractères dans le buffer d'écriture */
  nb = MIN(6 - *f_pos, count);

  if (copy_from_user(state.data_buffer + *f_pos, buf, nb) != 0) {
    return -EFAULT;
  }

  *f_pos += nb;

  if (*f_pos == 6) /* lorsque les 6 caractères ont été écrits, on peut mettre à jour les LEDs */
    update_rgb_from_buffer();

  return nb;
}

static void set_rgb(char *buffer) {
  int i;
  for (i=0; i < 6; i++) {
    state.data_buffer[i] = buffer[i];
  }
  update_rgb_from_buffer();
}


static void update_rgb_from_buffer(void) {
  state.r = hex2dec(state.data_buffer);
  state.g = hex2dec(state.data_buffer + 2);
  state.b = hex2dec(state.data_buffer + 4);
}

static void update_rgb_gpio(void) {
  int red_on = state.time < state.r;
  int green_on = state.time < state.g;
  int blue_on = state.time < state.b;
  state.time += ACCURACY;
  state.time %= 256;
  gpio_set_value(gpios[RED_LED].gpio, !red_on);
  gpio_set_value(gpios[GREEN_LED].gpio, !green_on);
  gpio_set_value(gpios[BLUE_LED].gpio, !blue_on);
}

static enum hrtimer_restart pwm_hrtimer_callback(struct hrtimer *timer)
{
  ktime_t now, ktime;
  update_rgb_gpio();
  now = hrtimer_cb_get_time(timer);
  ktime = ktime_set(0, PWM_PERIOD_NS);
  hrtimer_forward(timer, now, ktime);

  return HRTIMER_RESTART;
}

static int hex2dec(char *buffer) {
  int dec = 0;
  char c;
  int i;
  for (i = 0; i < 2; i++) {
    dec *= 16;
    c = buffer[i];
    if (c >= '0' && c <= '9') {
      dec += c - '0';
    } else if (c >= 'A' && c <= 'F') {
      dec += c - 'A' + 10;
    } else if (c >= 'a' && c <= 'f') {
      dec += c - 'a' + 10;
    }
  }
  return dec;
}

module_init(rgbled_init);
module_exit(rgbled_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Ludovic Schoepps, Mickaël Thomas");
MODULE_DESCRIPTION("RGB LED driver");
