# README

## Introduction

Ce module permet de commander une LED tricolore (RGB) reliée à un GPIO ainsi
que de réagir à l'action d'un bouton en éteignant cette même LED.

Il a été conçu pour une carte OLinuxino Maxi mais devrait pouvoir fonctionner
avec d'autres si leurs GPIOs sont supportés par Linux.

L'interaction avec l'espace utilisateur se fait via le fichier de périphérique 
`/dev/rgbled`.

## Schéma de montage

La LED RGB utilisée comporte 4 bornes :

 * R
 * G
 * B
 * +

Le borne + est reliée au +3.3V de la carte (borne 38 du connecteur GPIO).

Les bornes R, G, B sont reliées respectivement aux bornes 15, 17, 11
du connecteur par l'intermédiaire d'une resistance de limitation de courant
de 100 Ω pour chaque).

Des résistances de pull-down de 10 kΩ sont également cablées entre les bornes
15, 17, 11 et la masse (borne 40) pour éviter que la LED s'allume avant
l'initialisation des GPIO.

Le bouton est placé entre la masse et la borne 9, avec une résistance de
pull-up de 10 kΩ reliant le +3.3V à la borne 9.

## Utilisation

Pour utiliser ce module, il faut le charger via :

    modprobe rgbled

À ce moment, le module va réserver les gpio suivants :

 * 1 : pour la led verte
 * 2 : pour la led rouge
 * 4 : pour la led bleue
 * 5 : pour l'interrupteur

Le module va également créer le fichier `/dev/rgbled`

La couleur de la LED est représentée par une suite de trois nombres hexadécimaux
(entre 00 et FF) représentant respectivement l'intensité des composantes rouge,
verte et bleue.

Initialement, la LED restera éteinte. Pour changer la couleur de la LED,
il suffit d'écrire le code hexadécimal de la couleur souhaitée dans le fichier `/dev/rgbled`.
Il est également possible de récupérer la valeur actuelle en lisant depuis ce même fichier.

Toute action (appui ou le relachement) sur l'interrupteur va éteindre la LED
(à noter que la valeur lue reflète correctement cet état).

### Exemples d'utilisation

 * Eteint : `echo 000000 > /dev/rgbled`
 * Rouge : `echo FF0000 > /dev/rgbled`
 * Vert : `echo 00FF00 > /dev/rgbled`
 * Bleu : `echo 0000FF > /dev/rgbled`
 * Bleu tamisé : `echo 000020 > /dev/rgbled`
 * Blanc : `echo FFFFFF > /dev/rgbled`
 * Lecture de la valeur courante : `cat /dev/rgbled`

## Améliorations possibles

Rendre configurable (via les paramètres du module passés à modprobe) :

 * Les gpio utilisés
 * La fréquence du PWM
 * La précision du niveau de couleur RGB

Etudier la possibilité d'utiliser un contrôle matériel de la PWM.
