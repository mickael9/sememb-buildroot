################################################################################
#
# python-istdangerous
#
################################################################################

PYTHON_ITSDANGEROUS_VERSION = 0.23
PYTHON_ITSDANGEROUS_SOURCE = itsdangerous-$(PYTHON_ITSDANGEROUS_VERSION).tar.gz
PYTHON_ITSDANGEROUS_SITE = http://pypi.python.org/packages/source/i/itsdangerous
PYTHON_ITSDANGEROUS_LICENSE = BSD
PYTHON_ITSDANGEROUS_SETUP_TYPE = setuptools

$(eval $(python-package))
$(eval $(host-python-package))
