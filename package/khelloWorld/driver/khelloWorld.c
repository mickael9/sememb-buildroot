#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/gpio.h>
#include <linux/hrtimer.h>
#include <linux/ktime.h>

#define MS_TO_NS(x)	(x * 1E6L)

enum {
	RED_LED   = 0,
	GREEN_LED = 1,
	BLUE_LED  = 2
};

static struct gpio leds_gpios[] = {
    { 2, GPIOF_OUT_INIT_HIGH, "Red LED"   },
    { 1, GPIOF_OUT_INIT_HIGH, "Green LED" },
    { 4, GPIOF_OUT_INIT_HIGH, "Blue LED"  },
};

static struct hrtimer hr_timer;

static void set_rgb(int r, int g, int b) {
  gpio_set_value(leds_gpios[RED_LED].gpio, !r);
  gpio_set_value(leds_gpios[GREEN_LED].gpio, !g);
  gpio_set_value(leds_gpios[BLUE_LED].gpio, !b);
}

enum hrtimer_restart my_hrtimer_callback( struct hrtimer *timer )
{
  printk( "Callback called", jiffies );
  gpio_set_value(leds_gpios[RED_LED].gpio, 1);

  return HRTIMER_NORESTART;
}

static int __init tst_init(void)
{
  int err;

  printk(KERN_INFO"Hello world!\n");

  err = gpio_request_array(leds_gpios, ARRAY_SIZE(leds_gpios));
  if (err) {
    printk(KERN_ERR"Error requesting GPIOs");
    return -1;
  }

  gpio_set_value(leds_gpios[RED_LED].gpio, 0);

  //Lance un callback pour éteindre la LED rouge dans 1 sec
  ktime_t ktime = ktime_set( 0, MS_TO_NS(1000) );;

  hrtimer_init( &hr_timer, CLOCK_MONOTONIC, HRTIMER_MODE_REL );

  hr_timer.function = &my_hrtimer_callback;

  hrtimer_start( &hr_timer, ktime, HRTIMER_MODE_REL );

  return 0;
}

static void __exit tst_exit(void)
{
  printk(KERN_INFO"Goodbye world!\n");
  set_rgb(0, 0, 0);
  gpio_free_array(leds_gpios, ARRAY_SIZE(leds_gpios));
}

module_init(tst_init);
module_exit(tst_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("LS - MT");
MODULE_DESCRIPTION("KHello World!");
