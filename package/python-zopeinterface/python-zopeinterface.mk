################################################################################
#
# python-zopeinterface
#
################################################################################

PYTHON_ZOPEINTERFACE_VERSION = 4.1.0
PYTHON_ZOPEINTERFACE_SOURCE = zope.interface-$(PYTHON_ZOPEINTERFACE_VERSION).tar.gz
PYTHON_ZOPEINTERFACE_SITE = http://pypi.python.org/packages/source/z/zope.interface
PYTHON_ZOPEINTERFACE_SETUP_TYPE = distutils

define PYTHON_ZOPEINTERFACE_INSTALL_TARGET_CMDS
	(cd $(@D); \
    PYTHONPATH="$(TARGET_DIR)/usr/lib/python$(PYTHON_VERSION_MAJOR)/site-packages" \
    $(HOST_DIR)/usr/bin/python setup.py install --prefix=$(TARGET_DIR)/usr)
endef

$(eval $(python-package))
$(eval $(host-python-package))
