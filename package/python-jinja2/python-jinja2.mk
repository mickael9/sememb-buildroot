################################################################################
#
# python-jinja2
#
################################################################################

PYTHON_JINJA2_VERSION = 2.7.2
PYTHON_JINJA2_SOURCE = Jinja2-$(PYTHON_JINJA2_VERSION).tar.gz
PYTHON_JINJA2_SITE = http://pypi.python.org/packages/source/J/Jinja2
PYTHON_JINJA2_LICENSE = BSD
PYTHON_JINJA2_DEPENDENCIES = python-markupsafe host-python-markupsafe
PYTHON_JINJA2_SETUP_TYPE = setuptools

$(eval $(python-package))
$(eval $(host-python-package))
