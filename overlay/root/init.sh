#!/bin/sh
basegpio=/sys/class/gpio
for i in 1 2 4; do
	echo $i > $basegpio/export
	echo out > $basegpio/gpio${i}/direction
done
