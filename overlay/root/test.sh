#!/bin/sh

delay=400000

setLed () {
        echo $2 > ${basegpio}${1}/value
        usleep $delay
}

setLedInstant () {
        echo $2 > ${basegpio}${1}/value
}

basegpio=/sys/class/gpio/gpio
green=1
red=2
blue=4
while true; do
        setLed $green 1
        setLed $red 1
        setLed $green 0
        setLed $blue 1
        setLedInstant $green 1
        setLed $red 0
        setLed $green 0
        setLed $blue 0
done
