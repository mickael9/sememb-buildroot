#!/bin/sh

delay=400000

setLed () {
        echo $2 > ${basegpio}${1}/value
        usleep $delay
}

setLedInstant () {
        echo $2 > ${basegpio}${1}/value
}

basegpio=/sys/class/gpio/gpio
green=1
red=2
blue=4
while true; do
        rand=`tr -cd 0-6 </dev/urandom | head -c 1`
        rand2=`expr $rand % 2`
        rand=`expr $rand / 2`
        rand3=`expr $rand % 2`
        rand=`expr $rand / 2`
        setLedInstant $red $rand
        setLedInstant $green $rand2
        setLedInstant $blue $rand3
        usleep $delay
done
