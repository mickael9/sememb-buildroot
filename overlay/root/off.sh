#!/bin/sh
basegpio=/sys/class/gpio
for i in 1 2 4; do
	echo 1 > $basegpio/gpio${i}/value
	echo $i > $basegpio/unexport
done
