tohex() {
   printf "%X" $1
}
red=120
green=120
blue=120
for i in $(seq 0 1 255); do
    randR=`tr -cd 0-6 </dev/urandom | head -c 1`
    randG=`tr -cd 0-6 </dev/urandom | head -c 1`
    randB=`tr -cd 0-6 </dev/urandom | head -c 1`
    red=$((red + randR - 3))
    green=$((green + randG - 3))
    blue=$((blue + randB - 3))
    ./write.sh $(tohex $red) $(tohex $green) $(tohex $blue)
    usleep 5000
done
